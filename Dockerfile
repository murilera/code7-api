FROM python:3

WORKDIR /api

COPY requirements.* ./

RUN pip install -r requirements.txt $PIP_INSTALL_ARGS

COPY . .

CMD ["python", "api.py"]
