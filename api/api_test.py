import unittest
import json
from api import app


class ApiTestCase(unittest.TestCase):

    def test_addNews_invalid_404(self):
        tester = app.test_client(self)
        news = {"Teste": "Erro"}
        response = tester.post("/add", data=json.dumps(news))

        self.assertEqual(response.status_code, 400)
        self.assertTrue(b"Formato de noticia invalido:" in response.data)

    def test_updateNews_invalid_id(self):
        invalid_id = "489435494654"
        url = f"/update/{invalid_id}"
        tester = app.test_client(self)
        response = tester.put(url)

        self.assertEqual(response.status_code, 400)
        self.assertTrue(b"ID invalido" in response.data)

    def test_deleteNews_invalid_id(self):
        invalid_id = "46515164944642"
        url = f"/delete/{invalid_id}"
        tester = app.test_client(self)

        response = tester.delete(url)

        self.assertEqual(response.status_code, 400)
        self.assertTrue(b"ID invalido" in response.data)

if __name__ == "__main__":
    unittest.main()
