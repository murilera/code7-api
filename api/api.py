import helper
from flask import Flask, jsonify, request
from flask_pymongo import ObjectId, PyMongo


app = Flask(__name__)
app.config['MONGO_URI'] = 'mongodb://mongodb:27017/code7_api'
app.config['MONGO_DBNAME'] = 'code7_api'

mongo = PyMongo(app)
collection = mongo.db["code7_api"]


@app.route('/add', methods=['POST'])
def addNews():
    news = request.get_json(force=True)

    if helper.verify_keys(news) is False:
        return jsonify(f'Formato de noticia invalido: {news}'), 400

    title = news.get('title')
    content = news.get('content')
    author = news.get('author')

    news = {
        'title': title,
        'content': content,
        'author': author
    }

    result = collection.insert_one(news)
    return jsonify({"_id": str(result.inserted_id)}), 201


@app.route('/news', methods=['GET'])
def getAllNews():
    all_news = collection.find()
    output = []

    for news in all_news:
        output.append(helper.build_news_format(news))

    return jsonify(output)


@app.route('/search/<word>', methods=['GET'])
def searchNewsByWord(word):
    output = []
    search = helper.verify_search_word(word)

    if search:
        for news in search:
            output.append(helper.build_news_format(news))
        return jsonify(output)
    else:
        return jsonify('Nenhuma noticia foi encontrada'), 404


@app.route('/update/<news_id>', methods=['PUT'])
def updateNews(news_id):
    if not helper.verify_valid_object_id(news_id):
        return jsonify('ID invalido'), 400

    news = [n for n in collection.find({'_id': ObjectId(str(news_id))})]

    if news:
        news = request.get_json(force=True)
        collection.update_one({'_id': ObjectId(news_id)}, {'$set': news})

        new_news = collection.find_one({'_id': ObjectId(news_id)})
        output = helper.build_news_format(new_news)

        return jsonify(output), 202
    else:
        return jsonify('Nenhuma noticia foi encontrada'), 404


@app.route('/delete/<news_id>', methods=['DELETE'])
def deleteNews(news_id):
    if not helper.verify_valid_object_id(news_id):
        return f'ID invalido', 400

    news = [n for n in collection.find({'_id': ObjectId(str(news_id))})]

    if news:
        collection.delete_one(news[0])
        return jsonify(f'Noticia {news_id} deletada'), 202
    else:
        return jsonify(f'Noticia com id: {news_id} nao existe no banco'), 404


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
