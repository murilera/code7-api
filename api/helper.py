import api
from flask_pymongo import ObjectId

KEYS = ["title", "content", "author"]


def verify_keys(news):
    for k in KEYS:
        if k not in news:
            return False
    return True


def verify_search_word(word):
    if api.collection.find({"title": {'$regex': word, '$options': 'i'}}).count() > 0:
        title = api.collection.find({"title": {'$regex': word, '$options': 'i'}})
        return title
    if api.collection.find({"content": {'$regex': word, '$options': 'i'}}).count() > 0:
        content = api.collection.find({"content": {'$regex': word, '$options': 'i'}})
        return content
    if api.collection.find({"author": {'$regex': word, '$options': 'i'}}).count() > 0:
        author = api.collection.find({"author": {'$regex': word, '$options': 'i'}})
        return author
    return False


def verify_valid_object_id(news_id):
    try:
        if ObjectId(news_id):
            return news_id
    except:
        return False


def build_news_format(data):
    output = {
        'id': str(data['_id']),
        'title': data['title'],
        'content': data['content'],
        'author': data['author']
    }
    return output
