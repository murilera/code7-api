# About

Code7 Application Challange

# SETUP

* Install [docker](https://docs.docker.com/install/) and [docker-compose](https://docs.docker.com/compose/install/) on your machine

* Clone the repository & `cd` to the root directory

# RUN

Clone the project repository.

To build the Docker image, simply run:

```
docker build .
```

Using docker-compose run:

```
docker-compose up
```

# API

## Add news
```
curl -X POST http://localhost:5000/add -d '{"title":"title", "content":"content", "author": "author"}'
```

### Exampĺe
```
curl -X POST http://localhost:5000/add -d '''{
    "title": "Chapecó ultrapassa os 5.400 pacientes recuperados do Covid-19",
    "content": "Chapecó – De acordo com informações repassadas pela Administração de Chapecó na manhã de hoje, sexta-feira (21), o município já registrou desde o início da pandemia, 5.862 com Covid-19.",
    "author": "Michel"}'''

curl -X POST http://localhost:5000/add -d '''{
    "title": "title",
    "content": "content",
    "author": "author"}'''
```

## Read all news
```
curl http://localhost:5000/news
```

## Read one news
```
curl http://localhost:5000/search/[word]
```

### Exampĺe
```
curl http://localhost:5000/search/chape
```

## Update news (Get news_id by reading all or one news)
```
curl -X PUT http://localhost:5000/update/[id] -d '{"title":"title", "content":"content", "author": "author"}'
```

### Exampĺe
```
curl -X PUT http://localhost:5000/update/[news_id] -d '{"author": "Murilo"}'
```

## Delete news (Get news_id by reading all or one news)
```
curl -X DELETE http://localhost:5000/delete/[id]
```


### Flow Example
```
curl http://localhost:5000/news

curl -X POST http://localhost:5000/add -d '''{
    "title": "title",
    "content": "content",
    "author": "author"}'''

curl http://localhost:5000/search/title
curl http://localhost:5000/search/cont
curl http://localhost:5000/search/au

curl -X PUT http://localhost:5000/update/[id] -d '{"title":"titulo", "content":"conteudo", "author": "autor"}'

curl http://localhost:5000/news

curl -X DELETE http://localhost:5000/delete/[id]
```


# TODO

Tests [mangoose or unittest or pytest]
